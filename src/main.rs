#![warn(clippy::correctness, clippy::complexity, clippy::perf)]

extern crate clap;
extern crate select;

use clap::{App, Arg};
use zip;

use select::document::Document;
use select::predicate::Name;
use std::collections::HashMap;
use std::error::Error;
use std::fs;
use std::fs::File;
use std::io;
use std::io::Read;
use std::path::Path;
use std::process;

fn sanitize_string(input: &str) -> String
{
    let invalid = ['/', '\\', '?', '%', '*', ':', '|', '"', '<', '>'];

    input.chars()
         .map(|x| match &invalid.contains(&x)
         {
             true => '_',
             false => x,
         })
         .collect()
}

fn extract_files(path_archive: &Path,
                 path_dest: &Path,
                 links: &HashMap<String, String>)
                 -> Result<(), Box<dyn Error>>
{
    // https://github.com/mvdnes/zip-rs/blob/master/examples/extract.rs
    let mut archive = zip::read::ZipArchive::new(File::open(path_archive)?)?;

    println!("{}", path_dest.display());

    for (link_dest, link_text) in links.iter()
    {
        let mut file = archive.by_name(link_dest)?;
        let filename = sanitize_string(&link_text.trim_matches('.')
                                                 .to_string())
                       + "."
                       + link_dest.split('.').last().unwrap();
        let outpath = path_dest.join(Path::new(&filename));

        // println!("Extracting {} to {}", link_dest, outpath.display());
        println!("EXTRACT: {}\n", outpath.display());
        if (&*file.name()).ends_with('/')
        {
            fs::create_dir_all(&outpath)?;
        }
        else
        {
            if let Some(p) = outpath.parent()
            {
                if !p.exists()
                {
                    fs::create_dir_all(&p)?;
                }
            }
            let mut outfile = File::create(&outpath)?;
            io::copy(&mut file, &mut outfile)?;
        }
    }

    Ok(())
}

fn parse_toc_contents(toc_contents: &str)
                      -> Option<(String, HashMap<String, String>)>
{
    let doc = Document::from(toc_contents);

    let title = doc.find(Name("title")).next()?.text();
    let mut links = HashMap::new();
    for node in doc.find(Name("a"))
    {
        links.insert(node.attr("href")?.to_string(), node.text().to_string());
    }

    Some((title.to_string(), links))
}

fn get_toc_contents(path_archive: &Path) -> Result<String, Box<dyn Error>>
{
    let toc_filename = "Table of Contents.html";

    let mut archive = zip::read::ZipArchive::new(File::open(path_archive)?)?;
    let mut toc_zipped = archive.by_name(toc_filename)?;
    let mut toc_contents = String::new();
    toc_zipped.read_to_string(&mut toc_contents)?;

    Ok(toc_contents)
}

fn run(path_archive: &Path, path_dest: &Path) -> Result<(), Box<dyn Error>>
{
    let (title, links) = parse_toc_contents(&get_toc_contents(&path_archive)?)
        .ok_or("No links found in ToC")?;
    extract_files(&path_archive,
                  &path_dest.join(Path::new(&sanitize_string(&title))),
                  &links)?;

    Ok(())
}

fn main()
{
    let matches = App::new("d2l-extract")
        .version("0.1.0")
        .arg(
            Arg::with_name("archive")
                .short("a")
                .long("archive")
                .required(true)
                .index(1)
                .takes_value(true)
                .help("Input Zip archive"),
        )
        .arg(
            Arg::with_name("dest")
                .short("d")
                .long("destination")
                .required(true)
                .index(2)
                .takes_value(true)
                .help("Destination to extract files to"),
        )
        .get_matches();

    let arg_archive = Path::new(matches.value_of("archive").unwrap());
    let arg_dest = Path::new(matches.value_of("dest").unwrap());

    if let Err(e) = run(arg_archive, arg_dest)
    {
        println!("ERROR: {}", e);
        process::exit(1);
    }

    process::exit(0);
}

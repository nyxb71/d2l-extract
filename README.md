# d2l-extract

A small script to extract and automatically rename files downloaded from D2L content areas.

# Usage

`d2l-archive INPUT_ZIP_ARCHIVE OUTPUT_DIRECTORY`
